{
  config,
  lib,
  pkgs,
  ...
}:

{
  options.services.syndicate = lib.mkOption {
    default = { };
    example = {
      tty1 = {
        enable = true;
        user = "jane";
        config = [ "/etc/syndicate" ];
      };
    };
    description = ''
      Syndicate dataspace server instances.
      If the key is in the form of "tty''${N}" it will
      be attached to the appropriate teletypewriter.
    '';
    type = lib.types.attrsOf (
      lib.types.submodule {
        options = {
          enable = lib.mkEnableOption "this Syndicate dataspace server instance";

          user = lib.mkOption {
            type = lib.types.str;
            example = "jane";
            description = "User account under which the Syndicate server runs.";
          };

          package = lib.mkOption {
            default = pkgs.syndicate-server;
            defaultText = "pkgs.syndicate-server";
            type = lib.types.package;
            description = "The package to use for the Syndicate dataspace server.";
          };

          config = lib.mkOption {
            type = lib.types.listOf lib.types.path;
            description = "Configurations to load.";
            example = [ "/etc/syndicate" ];
          };
        };
      }
    );
  };

  config = {

    systemd.services =
      let
        configure = name: cfg: [
          {
            name = "syndicate-${name}";
            value =
              let
                serviceConfig = {
                  RuntimeDirectory = name;
                  ExecStart = "${cfg.package}/bin/syndicate-server --no-banner ${
                    lib.strings.concatMapStrings (path: " --config ${path}") cfg.config
                  }";
                  User = cfg.user;
                };
              in
              {
                description = "Syndicate dataspace server";
                restartIfChanged = false;
                reloadIfChanged = false;
                wantedBy = [ "multi-user.target" ];
              }
              // (
                if builtins.match "tty[0-9]" name == null then
                  { inherit serviceConfig; }
                else
                  {
                    after = [
                      "systemd-user-sessions.service"
                      "systemd-logind.service"
                      "getty@${name}.service"
                    ];
                    wants = [
                      "dbus.socket"
                      "systemd-logind.service"
                    ];
                    conflicts = [ "getty@${name}.service" ];
                    unitConfig.ConditionPathExists = "/dev/${name}";
                    serviceConfig = serviceConfig // {
                      PAMName = "login";
                      StandardError = "journal";
                      StandardInput = "tty-fail";
                      StandardOutput = "journal";
                      TTYPath = "/dev/${name}";
                      TTYReset = "yes";
                      TTYVHangup = "yes";
                      TTYVTDisallocate = "yes";
                      UtmpIdentifier = "%n";
                      UtmpMode = "user";
                      WorkingDirectory = "~";
                    };
                  }
              );
          }
        ];
        serverCfgs = lib.attrsets.filterAttrs (_: cfg: cfg.enable) config.services.syndicate;
      in
      lib.pipe serverCfgs [
        (lib.attrsets.mapAttrsToList configure)
        lib.lists.flatten
        builtins.listToAttrs
      ];

    systemd.targets.multi-user.wants = lib.lists.flatten (
      lib.attrsets.mapAttrsToList (
        name: cfg: lib.optional cfg.enable "syndicate-${name}.service"
      ) config.services.syndicate
    );
  };
}

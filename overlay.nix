final: prev:

{
  lib = prev.lib.extend (import ./lib.nix);

  acpi_actor = final.callPackage ./packages/acpi_actor { };

  fontconfig_actor = final.callPackage ./packages/fontconfig_actor { };

  libnotify_actor = final.callPackage ./packages/libnotify_actor { };

  nix-actor =
    final.callPackage
      (builtins.fetchTarball "https://git.syndicate-lang.org/ehmry/nix_actor/archive/trunk.tar.gz")
      { };

  noiseprotocol = final.callPackage ./packages/nim/noiseprotocol { };

  rrd-actor = final.callPackage ./packages/rrd-actor { };

  syndicated-open = final.callPackage ./packages/syndicated-open { };

  noise-c = final.callPackage ./packages/noise-c { };

  python3Packages = prev.python3Packages.overrideScope (
    final': prev': {
      preserves = final'.callPackage ./packages/preserves-py { };
      syndicate-py = final'.callPackage ./packages/syndicate-py { };
      synit-daemons = final'.callPackage ./packages/synit-daemons-py { };
    }
  );

  sqlite_actor = final.callPackage ./packages/sqlite_actor { };

  squeak = final.callPackage ./packages/squeak { };
  squeaker = final.python3Packages.callPackage ./packages/squeaker { };


  synit-pid1 = final.callPackage ./packages/synit-pid1 { };

  xapian_actor = final.callPackage ./packages/xapian_actor { };
}

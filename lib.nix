final: prev:
let
  lib = final;
in
with lib;
{

  generators =
    with final.generators;
    prev.generators
    // {
      toPreserves = throw "moved to https://git.syndicate-lang.org/ehmry/nix-processmgmt/src/branch/synit/nixproc/backends/synit";
    };
}

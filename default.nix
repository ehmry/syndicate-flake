{
  pkgs ? null,
}:
let
  overlay = import ./overlay.nix;
  pkgs' = if pkgs == null then import <nixpkgs> { overlays = [ overlay ]; } else pkgs.extend overlay;
in
builtins.intersectAttrs (overlay { } { }) pkgs'

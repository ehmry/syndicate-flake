# Syndicate Nix flake

Add as a Nix channel.
```sh
$ doas nix-channel --add https://git.syndicate-lang.org/ehmry/syndicate-flake/archive/trunk.tar.gz syndicate
```

To add to your local flake registry:
```sh
$ nix registry add syndicate "git+https://git.syndicate-lang.org/ehmry/syndicate-flake"
```

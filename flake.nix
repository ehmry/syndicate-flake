{
  description = "Syndicate utilities";

  inputs.nixpkgs.url = "nixpkgs/57610d2f8f09";

  outputs =

    { self, nixpkgs }:
    builtins.trace "Flakes are not recommended, use ./default.nix instead." {
      lib = nixpkgs.lib.extend (import ./lib.nix);

      overlays.default = import ./overlay.nix;

      legacyPackages = self.lib.mapAttrs (system: pkgs: pkgs.extend self.overlays.default) {
        inherit (nixpkgs.legacyPackages) aarch64-linux x86_64-linux;
      };

      packages = self.lib.mapAttrs (
        _: builtins.intersectAttrs (self.overlays.default { } { })
      ) self.legacyPackages;

      nixosModules.default = self.nixosModules.syndicate-server;
      nixosModules.syndicate-server =
        # A little hack to apply our overlay to this module only.
        {
          config,
          lib,
          pkgs,
          ...
        }:
        (import ./nixos/syndicate-server.nix) {
          inherit config lib;
          pkgs = pkgs.extend self.overlays.default;
        };

      devShells = self.lib.mapAttrs (
        system: pkgs: with pkgs; {
          default = mkShell { packages = builtins.attrValues self.packages.${system}; };
        }
      ) self.legacyPackages;

      checks = self.lib.mapAttrs (
        system: pkgs':
        with import (nixpkgs + "/nixos/lib/testing-python.nix") {
          inherit system;
          pkgs = pkgs';
        }; {
          simple = simpleTest {
            name = "http";
            nodes.machine =
              { config, pkgs, ... }:
              {
                imports = [ self.nixosModules.syndicate-server ];

                nixpkgs.pkgs = pkgs';

                services.syndicate.tty1 = {
                  enable = true;
                  user = "loser";
                  config = [ ];
                };
                users.users.loser.isNormalUser = true;
              };
            testScript = ''
              machine.wait_for_job("syndicate-tty1")
            '';
          };
        }
      ) { inherit (self.legacyPackages) x86_64-linux; };
    };
}

{
  lib,
  buildPythonPackage,
  fetchPypi,
  setuptools-scm,
  preserves,
  websockets,
}:

buildPythonPackage rec {
  pname = "syndicate-py";
  version = "0.15.0";

  src = fetchPypi {
    inherit pname version;
    hash = "sha256-ePCjzy5ros4H3rp0EszjUiUMCbPmNLWlXCaYcqSui0k=";
  };

  buildInputs = [ setuptools-scm ];
  propagatedBuildInputs = [
    preserves
    websockets
  ];

  meta = src.meta // {
    description = "Syndicated Actor model for Python";
    homepage = "https://syndicate-lang.org";
    maintainers = with lib.maintainers; [ ehmry ];
  };
}

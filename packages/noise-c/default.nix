{
  lib,
  stdenv,
  fetchFromGitHub,
  autoreconfHook,
}:

stdenv.mkDerivation rec {
  pname = "noise-c";
  version = "20230309";
  outputs = [
    "bin"
    "out"
  ];
  src = fetchFromGitHub {
    owner = "rweather";
    repo = pname;
    rev = "ffa626bddd1f8182e47c634af686108ca5049e56";
    hash = "sha256-HkcyV/WT4UNFWczbZZIDW2fWRliQb8IEykv2d7bZM7w=";
  };

  nativeBuildInputs = [ autoreconfHook ];

  # excise protobuf bloat
  prePatch = ''
    substituteInPlace tools/Makefile.am \
      --replace 'SUBDIRS = keytool protoc' 'SUBDIRS = keytool'
  '';

  meta = src.meta // {
    description = "Plain C implementation of the Noise Protocol";
    license = lib.licenses.mit;
    maintainers = with lib.maintainers; [ ehmry ];
  };
}

{
  lib,
  buildPythonPackage,
  fetchPypi,
  python,
  setuptools-scm,
}:

buildPythonPackage rec {
  pname = "preserves";
  version = "0.992.2";
  pyproject = true;

  src = fetchPypi {
    inherit pname version;
    hash = "sha256-VO9kN/nSB5K0LbpY6xjxgIUhZye/rX8gRxlHomUPRRk=";
  };

  nativeBuildInputs = [ setuptools-scm ];

  meta = src.meta // {
    description = "Preserves serialization format";
    homepage = "https://preserves.dev/";
    maintainers = with lib.maintainers; [ ehmry ];
  };
}

{
  lib,
  buildNimPackage,
  fetchFromGitea,
  pkg-config,
  syndicate,
  xapian,
}:

buildNimPackage rec {
  pname = "xapian_actor";
  version = "20230610";

  src = fetchFromGitea {
    domain = "git.syndicate-lang.org";
    owner = "ehmry";
    repo = pname;
    rev = version;
    hash = "sha256-f/+l+c6SCvHH8zDz3/9ndrT77ck5gfk5zESKL74IfNg=";
  };

  propagatedNativeBuildInputs = [ pkg-config ];
  propagatedBuildInputs = [
    syndicate
    xapian
  ];

  meta = src.meta // {
    maintainers = [ lib.maintainers.ehmry ];
    license = lib.licenses.unlicense;
  };
}

{
  lib,
  buildNimPackage,
  fetchFromGitea,
}:

buildNimPackage (finalAttrs: {
  pname = "acpi_actor";
  version = "20231010";

  src = fetchFromGitea {
    domain = "git.syndicate-lang.org";
    owner = "ehmry";
    repo = pname;
    rev = finalAttrs.version;
    hash = "sha256-eN3VC2yL6UvjeIGvzx0PAujR/Xd6Rc1e9Ftf6fKKk2E=";
  };

  lockFile = "${src}/lock.json";

  meta = src.meta // {
    maintainers = [ lib.maintainers.ehmry ];
    license = lib.licenses.unlicense;
  };
})

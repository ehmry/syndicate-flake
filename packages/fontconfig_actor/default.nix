{
  lib,
  buildNimPackage,
  fetchFromGitea,
  fontconfig,
  pkg-config,
}:

buildNimPackage rec {
  pname = "fontconfig_actor";
  version = "20231021";

  src = fetchFromGitea {
    domain = "git.syndicate-lang.org";
    owner = "ehmry";
    repo = pname;
    rev = version;
    hash = "sha256-3LRIsZ6NA+E799/z6PGb4wH3CgJ6lx2CgKH/CzEuSzQ=";
  };

  nativeBuildInputs = [ pkg-config ];
  buildInputs = [ fontconfig ];

  lockFile = "${src}/lock.json";

  meta = src.meta // {
    description = "Syndicate actor for asserting Fontconfig information";
    maintainers = [ lib.maintainers.ehmry ];
    license = lib.licenses.unlicense;
  };
}

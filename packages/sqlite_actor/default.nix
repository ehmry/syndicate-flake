{
  lib,
  buildNimPackage,
  fetchFromGitea,
  pkg-config,
  sqlcipher,
}:

buildNimPackage rec {
  pname = "sqlite_actor";
  version = "20231010";

  src = fetchFromGitea {
    domain = "git.syndicate-lang.org";
    owner = "ehmry";
    repo = pname;
    rev = version;
    hash = "sha256-oKw5GUsK1F2RegOFiNYkLXT5CvtWcF/8H6QdE4DXa1A=";
  };

  propagatedNativeBuildInputs = [ pkg-config ];
  propagatedBuildInputs = [ sqlcipher ];

  lockFile = "${src}/lock.json";

  meta = src.meta // {
    maintainers = [ lib.maintainers.ehmry ];
    license = lib.licenses.unlicense;
  };
}

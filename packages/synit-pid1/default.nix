# https://git.syndicate-lang.org/synit/synit/src/branch/main/packaging/packages/synit-pid1/APKBUILD

{
  lib,
  rustPlatform,
  rustc,
  fetchFromGitea,
}:

rustPlatform.buildRustPackage rec {
  pname = "synit-pid1";
  version = "unstable-2023-11-01";
  src = fetchFromGitea {
    domain = "git.syndicate-lang.org";
    owner = "synit";
    repo = "synit";
    rev = "a2ecd8a4e441f8622a57a99987cb0aa5be9e1dcd";
    hash = "sha256-M79AJ8/Synzm4CYkt3+GYViJLJcuYBW+x32Vfy+oFUM=";
  };

  patchPhase =
    # Patch to take children and configuration from /run/booted-system.
    ''
      runHook prePatch
      substituteInPlace src/main.rs \
        --replace '"/usr/bin/syndicate-server"' '"/run/booted-system/sw/bin/syndicate-server"' \
        --replace '"/sbin/synit-log"' '"/run/booted-system/sw/bin/synit-log"' \
        --replace '"/etc/syndicate/boot"' '"/run/booted-system/etc/syndicate/boot"' \

      runHook postPatch
    '';

  sourceRoot = "source/${pname}";
  cargoHash = "sha256-mO1gMtXEc34v9zBst1by1Ed8l/kvuJ9n4/6vJ1LhijM=";
  nativeBuildInputs = [ rustc ];

  RUSTC_BOOTSTRAP = true;

  meta = src.meta // {
    description = "Synit pid 1 program (patched for NixOS)";
    homepage = "https://synit.org/";
    maintainers = with lib.maintainers; [ ehmry ];
  };
}

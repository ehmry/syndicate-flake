{
  lib,
  python3Packages,
  fetchFromGitea,
}:

python3Packages.buildPythonApplication rec {
  pname = "synit-python-daemons";
  version = "2023-11-01";
  format = "other";

  src = fetchFromGitea {
    domain = "git.syndicate-lang.org";
    owner = "synit";
    repo = "synit";
    rev = "a2ecd8a4e441f8622a57a99987cb0aa5be9e1dcd";
    hash = "sha256-M79AJ8/Synzm4CYkt3+GYViJLJcuYBW+x32Vfy+oFUM=";
  };

  sourceRoot = "${src.name}/packaging/packages/synit-config/files/usr/lib/synit/python/synit/daemon";

  propagatedBuildInputs = with python3Packages; [
    pyroute2
    syndicate-py
  ];

  postPatch = ''
    for f in *.py; do
      substituteInPlace $f --replace \
        "/home/tonyg/src/synit/protocols" \
        "$out/share/syndicate-protocols"
    done
  '';

  buildPhase = ''
    runHook preBuild
    for f in *.py; do
      sed -i '1i #!/bin/python' $f
    done
    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall
    for f in *.py; do
      bin_name=$f
      bin_name=''${bin_name/.py}
      bin_name=''${bin_name//_/-}
      install --mode 0555 -D $f $out/bin/$bin_name
    done
    install -D -t $out/share/syndicate-protocols $src/protocols/schema-bundle.bin
    runHook postInstall
  '';
}

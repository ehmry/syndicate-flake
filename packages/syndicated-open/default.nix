{
  lib,
  buildNimPackage,
  fetchFromGitea,
  makeDesktopItem,
  pkg-config,
  pcre,
}:

buildNimPackage rec {
  pname = "syndicated-open";
  version = "20231010";

  src = fetchFromGitea {
    domain = "git.syndicate-lang.org";
    owner = "ehmry";
    repo = pname;
    rev = version;
    hash = "sha256-DVBSsnN59XKV7Pc1spBxcLwyOIif3xMHXHID9khVJck=";
  };

  nativeBuildInputs = [ pkg-config ];
  propagatedBuildInputs = [ pcre ];

  lockFile = "${src}/lock.json";

  desktopItem = makeDesktopItem rec {
    name = "open";
    desktopName = "Syndicated URI open";
    exec = "${name} %U";
    mimeTypes = [
      "application/vnd.mozilla.xul+xml"
      "application/xhtml+xml"
      "text/html"
      "text/xml"
      "x-scheme-handler/http"
      "x-scheme-handler/https"
    ];
  };

  postInstall = ''
    ln -s open $out/bin/xdg-open
    cp -a $desktopItem/* $out/
  '';

  meta = src.meta // {
    description = "Syndicated open command";
    maintainers = [ lib.maintainers.ehmry ];
    license = lib.licenses.unlicense;
  };
}

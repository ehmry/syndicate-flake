{
  lib,
  buildNimPackage,
  fetchFromGitea,
  libnotify,
  pkg-config,
}:

buildNimPackage rec {
  pname = "libnotify_actor";
  version = "20231130";

  src = fetchFromGitea {
    domain = "git.syndicate-lang.org";
    owner = "ehmry";
    repo = pname;
    rev = version;
    hash = "sha256-PNRscNm3axhEr1O2QGLs9JBH35err34y3wc9arPxZ5c=";
  };

  propagatedNativeBuildInputs = [ pkg-config ];
  propagatedBuildInputs = [ libnotify ];

  lockFile = "${src}/lock.json";

  meta = src.meta // {
    maintainers = [ lib.maintainers.ehmry ];
    license = lib.licenses.unlicense;
  };
}

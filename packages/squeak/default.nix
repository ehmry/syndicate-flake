{
  lib,
  stdenv,
  fetchurl,
  autoPatchelfHook,
  makeWrapper,
  unzip,
  alsa-lib,
  libGL,
  libpulseaudio,
  libuuid,
  nas,
  openssl,
  pango,
  sndio,
  xorg,
}:

let
  versionA = "6.0";
  versionB = "22104";
  versionC = "202206021410";
  bits =
    with stdenv.hostPlatform;
    if is32bit then
      "32"
    else if is64bit then
      "64"
    else
      throw "too many addressing bits";
  sources = {
    i686-linux = fetchurl {
      url = "http://files.squeak.org/${versionA}/Squeak${versionA}-${versionB}-${bits}bit/Squeak${versionA}-${versionB}-${bits}bit-${versionC}-Linux-x86.tar.gz";
      sha256 = "sha256-6o9GCeNsHV+OhHBUCqBmslgUEBrS3XPwsl/p8zgfJQs=";
    };
    x86_64-linux = fetchurl {
      url = "http://files.squeak.org/${versionA}/Squeak${versionA}-${versionB}-${bits}bit/Squeak${versionA}-${versionB}-${bits}bit-${versionC}-Linux-x64.tar.gz";
      sha256 = "sha256-pgTdFyqAQo3K6Th2FXL8Op1hakjcL4rJ6wnKuIlFZ9g=";
    };
  };
in
stdenv.mkDerivation rec {
  pname = "squeak";
  version = "${versionA}-${versionB}-${versionC}";

  src = sources.${stdenv.system};

  nativeBuildInputs = [
    autoPatchelfHook
    makeWrapper
    unzip
  ];

  buildInputs = with xorg; [
    alsa-lib
    libGL
    libICE
    libSM
    libX11
    libXext
    libXrender
    libpulseaudio
    libuuid
    nas
    pango
    sndio
  ];

  dontBuild = true;

  installPhase = ''
    mkdir -p $out/bin
    cp -a bin $out/lib
    cp -a shared/* $out/lib
    makeWrapper $out/lib/squeak $out/bin/squeak \
      --prefix LD_LIBRARY_PATH ":" "$out/lib:${lib.makeLibraryPath [ openssl ]}" \
      --set SQUEAK_IMAGE $out/lib/Squeak${versionA}-${versionB}-${bits}bit.image
  '';

  preFixup = ''
    patchelf $out/lib/vm-sound-sndio.so \
      --replace-needed libsndio.so.6.1 libsndio.so
  '';
  meta = {
    description = "Squeak virtual machine and image";
    homepage = "https://squeak.org/";
    license = with lib.licenses; [
      asl20
      mit
    ];
    maintainers = with lib.maintainers; [ ehmry ];
    platforms = builtins.attrNames sources;
  };
}

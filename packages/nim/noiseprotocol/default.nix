{
  lib,
  buildNimPackage,
  noise-c,
}:

buildNimPackage rec {
  pname = "noise";
  version = "20230509";

  src = null;

  propagatedBuildInputs = [ noise-c ];

  meta = {
    inherit (noise-c.meta) license;
    maintainers = [ lib.maintainers.ehmry ];
  };
}

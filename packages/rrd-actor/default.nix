{
  lib,
  buildNimSbom,
  fetchFromGitea,
  pkg-config,
  rrdtool,
}:

let
  version = "20241231";
  src = fetchFromGitea {
    domain = "git.syndicate-lang.org";
    owner = "ehmry";
    repo = "rrd_actor";
    rev = version;
    hash = "sha256-f4cQkjG/OX2u1ybYbp/lDGEvIjHN5qIOrbVhwOz6b4Y=";
  };
in
buildNimSbom (finalAttrs: {
  pname = "rrd-actor";
  inherit src version;
  nativeBuildInputs = [ pkg-config ];
  buildInputs = [
    rrdtool
  ];
  nimFlags = [ "--define:nimPreviewHashRef" ];
  meta = finalAttrs.src.meta // {
    description = "Syndicated RRDTool";
    homepage = "https://git.syndicate-lang.org/ehmry/rrd_actor";
    maintainers = [ lib.maintainers.ehmry ];
    mainProgram = "rrd-actor";
    license = lib.licenses.unlicense;
  };
}) "${src}/sbom.json"
